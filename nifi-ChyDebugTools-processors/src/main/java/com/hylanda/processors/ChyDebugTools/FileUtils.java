package com.hylanda.processors.ChyDebugTools;

import org.apache.nifi.logging.ComponentLog;

import java.io.*;

public class FileUtils {
    public static ComponentLog _logger = null;
    //private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
    public static String Tailn(String strFile,int nLine) {
        String cmd =String.format("tail -n %1$d /data/logs/%2$s",nLine,strFile);
        _logger.info("执行命令"+cmd);
        String []cmdarry ={"/bin/bash", "-c", cmd};
        return exec(cmdarry);
    }
    protected static String exec(String []cmdarry) {
        StringBuffer returnString = new StringBuffer();
        Process pro = null;
        Runtime runTime = Runtime.getRuntime();
        if (runTime == null) {
            System.err.println("Create runtime false!" );
        }
        try {
            pro = runTime.exec(cmdarry);
            BufferedReader input = new BufferedReader(new InputStreamReader(pro.getInputStream()));
           // PrintWriter output = new PrintWriter(new OutputStreamWriter(pro.getOutputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                _logger.info(line);
                returnString.append(line);
                returnString.append("\n");
            }
            _logger.info(returnString.toString());
            input.close();
           // output.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally {
            if (pro != null){
                pro.destroy();
            }
        }
        return returnString.toString();
    }

}