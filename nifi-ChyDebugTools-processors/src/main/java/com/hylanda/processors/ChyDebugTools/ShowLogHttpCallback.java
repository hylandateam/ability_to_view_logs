package com.hylanda.processors.ChyDebugTools;


import fi.iki.elonen.NanoHTTPD;

import java.io.File;
import java.util.List;
import java.util.Map;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class ShowLogHttpCallback implements UnitHttpCallBackI
{
    public static String HTMLEncode(String input){
        return input.replace("&","&amp;")
                .replace(">", "&gt;")
                .replace( "<", "&lt;")
                .replace(" ","&nbsp;")
                .replace("\"", "&quot;")
                .replace("\n","<br />")
                .replace("\r","");
    }

    String HTMLHEAD = "<!DOCTYPE html>\n" +
            "<!--[if IE 8]> <html lang=\"cn\" class=\"ie8 no-js\"> <![endif]-->\n" +
            "<!--[if IE 9]> <html lang=\"cn\" class=\"ie9 no-js\"> <![endif]-->\n" +
            "<!--[if !IE]><!-->\n" +
            "<html lang=\"cn\" class=\"no-js\">\n" +
            "<!--<![endif]-->\n" +
            "\t<!-- BEGIN HEAD -->\n" +
            "\t<head>\n" +
            "        <meta charset=\"utf-8\" />" +
            "\t</head>" +
            "\t<body>";
    String HTMLEND= "</body>";
    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session)
    {
        // TODO Auto-generated method stub
        NanoHTTPD.Method method = session.getMethod();
        String uri = session.getUri();
        if (method.equals(NanoHTTPD.Method.GET))
        {
            if (uri.equalsIgnoreCase("/httpshowlog"))
            {
                Map<String, List<String>> params = session.getParameters();
                List<String> f = params.get("f");
                if (f != null && !f.isEmpty())
                {
                    String file = f.get(0);
                    int nLine = 50;
                    List<String> l = params.get("l");
                    if (l != null && !l.isEmpty()){
                        try {
                            nLine = Integer.parseInt(l.get(0));
                        }catch (NumberFormatException e){

                        }
                    }
                    nLine = Math.min(nLine,500);
                    String content = HTMLHEAD+"<a href=\"show-page\">返回<a><br />";
                    content += HTMLEncode(FileUtils.Tailn(file,nLine));
                    content += HTMLEND;
                    NanoHTTPD.Response response = newFixedLengthResponse(NanoHTTPD.Response.Status.OK, NanoHTTPD.MIME_HTML,content);
                    return response;

                }

            }
            if (uri.equalsIgnoreCase("/show-page"))
            {
                String path = "/data/logs";
                File file = new File(path);
                File[] fs = file.listFiles();
                String content = HTMLHEAD;
                for(File f:fs){
                    if(!f.isDirectory()) {
                        content += String.format("<a href=\"httpshowlog?f=%1$s\">%2$s</a><br />\n",f.getName(),f.getName());
//                        content += f.getPath() + " - " + f.getName() + "<br />";
                    }
                }
                content += HTMLEND;
                NanoHTTPD.Response response = newFixedLengthResponse(NanoHTTPD.Response.Status.OK, NanoHTTPD.MIME_HTML,content);
                return response;
            }
        }
        return null;
    }
}

