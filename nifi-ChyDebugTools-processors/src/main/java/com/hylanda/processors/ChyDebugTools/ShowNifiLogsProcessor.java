/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hylanda.processors.ChyDebugTools;

import com.hylanda.psc.tools.ProxyTools;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.state.Scope;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.util.LogAppendInfoUtil;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;


@CustomDataPreferred
@ShowState(keys = { @StateKey(key = "提示") })
@Tags({"example"})
@CapabilityDescription("Provide a description")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute = "", description = "")})
@WritesAttributes({@WritesAttribute(attribute = "", description = "")})
public class ShowNifiLogsProcessor extends AbstractProcessor {
    //private static final Logger logger = LoggerFactory.getLogger(ShowNifiLogsProcessor.class);
    //    public static final PropertyDescriptor MY_PROPERTY = new PropertyDescriptor
//            .Builder().name("ExportLimit")
//            .displayName("导出日志量")
//            .description("导出日志量：\n少=仅最近日志且最大每个日志最大1M\n中=仅当天日志且每个日志最大5M\n大=仅当天日志每个日志无限制（慎用后果不负责）\n全部（慎用后果不负责）")
//            .required(true)
//            .defaultValue("少")
//            .allowableValues("少", "中", "大", "全部")
//            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
//            .build();
//
//    public static final Relationship MY_RELATIONSHIP = new Relationship.Builder()
//            .name("MY_RELATIONSHIP")
//            .description("Example relationship")
//            .build();
    UnitHttp _httpSvr = null;
    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships = new HashSet<>();
    private AtomicBoolean _running = new AtomicBoolean(false);

    @Override
    protected void init(final ProcessorInitializationContext context) {
        FileUtils._logger = this.getLogger();
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        // descriptors.add(MY_PROPERTY);
        this.descriptors = Collections.unmodifiableList(descriptors);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) throws IOException {
        if (false == _running.getAndSet(true)) {
            if (_httpSvr != null) {
                _httpSvr.stop();
            }
            int nPort = ProxyTools.getFreePort(20000, 30000);
            this.getLogger().info("log service listen on port : " + String.valueOf(nPort));
            _httpSvr = new UnitHttp(nPort);
            _httpSvr.AddCallBack(new ShowLogHttpCallback());
            try {
                _httpSvr.start();
                this.getLogger().info("展示日志服务启动成功");
                String projectId = LogAppendInfoUtil.projectId();
                String userId = LogAppendInfoUtil.userId();
                String showUrl = ProxyTools.getSecretProxyUrl("slog",userId,projectId,nPort);
                if(showUrl != null && !showUrl.isEmpty()) {
                    this.getLogger().info("get secret proxy url = " + showUrl);
                    context.getStateManager().setState(new HashMap<String, String>() {{
                        put("提示", "请点击右侧数据标签查看日志");
                        put("data_url", showUrl);
                    }}, Scope.LOCAL);
                }else{
                    this.getLogger().error("get secret proxy url emtpy ");
                    context.getStateManager().setState(new HashMap<String, String>() {{
                        put("提示", "获取日志查看授权失败");
                        put("data_url", "");
                    }}, Scope.LOCAL);
                }

            } catch (IOException e) {
                this.getLogger().error("展示日志服务启动失败" + e.toString());
                e.printStackTrace();
                context.getStateManager().setState(new HashMap<String, String>() {{
                    put("提示", "启动失败");
                    put("data_url", "");
                }}, Scope.LOCAL);
            }
        }
    }


    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {

    }

    @OnStopped
    public void onStopped(final ProcessContext context) throws IOException {
        this.getLogger().info("展示日志服务停止");
        if (_httpSvr != null) {
            _httpSvr.stop();
            _httpSvr = null;
            _running.set(false);
        }
    }


}
