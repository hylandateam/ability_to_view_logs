package com.hylanda.processors.ChyDebugTools;


import java.io.IOException;
import java.util.ArrayList;


import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

public class UnitHttp extends NanoHTTPD
{
    //private static Logger LOGGER = LoggerFactory.getLogger(UnitHttp.class);
    ArrayList<UnitHttpCallBackI> lstCalls = new ArrayList<UnitHttpCallBackI>();

    public UnitHttp( int nPort)
    {
        super(nPort);
        FileUtils._logger.info(" Http Server Started!");
    }

    public void AddCallBack(UnitHttpCallBackI callback)
    {
        lstCalls.add(callback);
    }

    @Override
    public Response serve(IHTTPSession session)// (String uri, String method,
    // Properties header, Properties
    // parms, Properties files)
    {
        Response res404 = super.serve(session);
        FileUtils._logger.info("Accept Http Quest: " + session.getUri());
        try
        {
//			System.out.println(session.getMethod().toString());
//			System.out.println(session.getUri().toString());
//			System.out.println(session.getHeaders().toString());
//			System.out.println(session.getInputStream().toString());
//			System.out.println(session.getParms().toString());
//			System.out.println(session.getQueryParameterString());
            for (int i = 0; i < lstCalls.size(); i++)
            {
                Response res = lstCalls.get(i).serve(session);
                if (res != null)
                {
                    return res;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return newFixedLengthResponse(Status.INTERNAL_ERROR, MIME_PLAINTEXT, e.toString());

        }
        return res404;
    }

    public static void main(String[] args) throws IOException
    {
        // TODO Auto-generated method stub
        UnitHttp hhh = new UnitHttp( 12004);
        hhh.AddCallBack(new ShowLogHttpCallback());
        hhh.start();
        System.out.println("Listening on port 12001. Hit Enter to stop.\n");
        try
        {
            System.in.read();
        }
        catch (Throwable t)
        {
        }
        ;
        hhh.stop();
        hhh = new UnitHttp( 12004);
        hhh.AddCallBack(new ShowLogHttpCallback());
        hhh.start();
        System.out.println("Listening on port 12001. Hit Enter to stop.\n");
        try
        {
            System.in.read();
        }
        catch (Throwable t)
        {
        }
        ;
        hhh.stop();
    }

}
