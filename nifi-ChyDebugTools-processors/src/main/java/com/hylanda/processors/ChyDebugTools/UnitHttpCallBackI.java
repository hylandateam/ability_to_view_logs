package com.hylanda.processors.ChyDebugTools;

import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;

public interface UnitHttpCallBackI
{
    //无匹配则返回null
    public Response serve(IHTTPSession session);
}
