package com.hylanda.psc.tools;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.Random;

public class ProxyTools {

    public final static String PSC_SERVICE_API = "http://powerstore.hylanda.com/psc/api";
    //public final static String PSC_SERVICE_API = "http://10.25.84.204:20002/api";
    public static String getSecretProxyUrl(String myid,String user_id, String project_id ,int port){
        try {
            JsonRpcHttpClient client = new JsonRpcHttpClient(new URL(PSC_SERVICE_API));
            client.setConnectionTimeoutMillis(10000);
            client.setReadTimeoutMillis(30000);

            String strRet = client.invoke("project.get_proxy_secret",
                    new Object[] {myid,user_id,project_id,port}, String.class);
            if (strRet != null && !strRet.isEmpty()){

                String url = strRet;
                //System.out.println(url);
                return url;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return "";
    }

    public static int getFreePort(int nMinPort, int nMaxPort) {//获取空闲端口
        Random r = new Random();
        for (int i = nMinPort; i <= nMaxPort; i++) {
            int port = nMinPort + r.nextInt(nMaxPort - nMinPort);
            String host = System.getenv().get("DOCKER_HOST_ADDR");
            if (host == null || host.isEmpty()) {
                host = "127.0.0.1";
            }
            Socket socket = new Socket();
            try {
                socket.connect(new InetSocketAddress(host, port));
            } catch (IOException e) {
                //e.printStackTrace();
                return port;
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
        return -1;
    }
}
